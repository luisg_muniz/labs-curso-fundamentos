---
title: 01 Preparación de entornos
parent: Laboratorios
---
<details open markdown="block">
  <summary>Sumario</summary>
  {: .text-delta }
1. TOC
{:toc}
</details>



# Objetivo

Los laboratorios del curso van a hacerse empleando [VirtualBox](https://www.virtualbox.org/wiki).  En este primer laboratorio, nos aseguraremos de que está correctamente instalado, que la configuración de la red es correcta, y haremos la importación de la primera máquina virtual.



# Prerrequisitos

Para completar este laboratorio, necesitaremos:
* Conexión a <a href="https://www.youtube.com/watch?v=BBqQLOor0Rk&t=24s" target="_blank">Internet</a>
* [Descargar VirtualBox](https://www.virtualbox.org/wiki/Downloads) (incluyendo el "Oracle VM VirtualBox **Extension Pack**")
* Descargar la primera [máquina virtual del curso](https://bit.ly/3FzwXhL)


# Pasos

## Descripción

En VirtualBox podemos no sólo crear (e importar) máquinas virtuales, sino también simular la infraestructura necesaria.
* En especial, hay varios tipos de redes virtuales (con o sin [DHCP](https://es.wikipedia.org/wiki/Protocolo_de_configuraci%C3%B3n_din%C3%A1mica_de_host), con o sin encaminador (_router_) de salida) que permiten interconectar, de diversas formas, las distintas máquinas virtuales.



<br>
<br>
## Revisión de VirtualBox

01. Lanzamos el programa VirtualBox.

02. Verificamos que estemos usando una versión 6.x o bien 7.x: menú `Ayuda > Acerca de VirtualBox...` (en Mac, menú `VirtualBox > Acerca de VirtualBox` )

> Nota: A fecha de hoy, la versión más reciente es la 7.0.4.  **No** es necesario que actualicemos VirtualBox, si no queremos.

![(versión de VirtualBox](https://www.muycomputer.com/wp-content/uploads/2022/10/VirtualBox-7-Final_2.jpg)


03. Es conveniente que estén instaladas las extensiones "Oracle VM VirtualBox Extension Pack", y con la **misma versión** que el programa.  Lo comprobamos en el menú `Archivo > Herramientas > Extension Pack Manager`[^vb6-a]
    * Si es necesario, podemos descargar y añadir el "Oracle VM VirtualBox Extension Pack" desde la misma página de [descargas de VirtualBox](https://www.virtualbox.org/wiki/Downloads)
    * Si tenemos una versión algo anterior de VirtualBox, podemos descargar su "Extension Pack" correspondiente desde la sección [VirtualBox older builds](https://www.virtualbox.org/wiki/Download_Old_Builds)

![(El Extension Pack)](https://img.creativemark.co.uk/uploads/images/638/12638/largeImg.png)


<br>
<br>
## Arquitectura de los laboratorios del curso

En los distintos laboratorios del curso llegaremos a usar varias máquinas virtuales.  Estarán interconectadas a través de una red común entre ellas.
Esa red será también accesible desde la máquina anfitriona; así, podremos verificar más fácilmente el resultado de los despliegues hechos mediante Ansible.

![Arquitectura Labs](../../../attached_files/images/arquitectura-labs.png)



<br>
<br>
## Red "Sólo Anfitrión" (_Host-Only_)

01. Revisamos si ya existe alguna red interna: menú `Archivo > Herramientas > Network Manager`[^vb6-b]
    * Si el listado está **vacío** (que será lo habitual), o si **ninguna** de las posibles redes tienen asignada el rango IPv4 que nos interesa **(192.168.56.X/24)**, continuaremos en el paso #2
    * Si el listado **sí** tiene alguna red con la dirección IPv4 correcta (192.168.56.1/24) anotaremos su nombre porque, en el resto de las instrucciones de este laboratorio, donde se diga "VirtualBox Host-Only Ethernet Adapter0" en realidad usaremos ese otro nombre ("VirtualBox Host-Only Ethernet Adapter XXX").  Continuamos con la sección [Importación de "sys0"](#Importación de "sys0").
    
02. Pulsamos en el icono "Crear" (tarjeta de red con un símbolo "+" verde).  Aparecerá una nueva red:

    | Nombre   | Dirección/máscara IPv4 | Dirección/máscara IPv6 | Servidor DHCP       |
    |----------|:----------------------:|:----------------------:|--------------------:|
    | VirtualBox Host-Only Ethernet Adapter |    192.168.56.1/24     |            -           | ☐ (Deshabilitado)   |

03. Salimos de la ventana pulsando en `Cerrar`.


<br>
<br>
## Importación de "sys0"

Desde el [enlace de descarga](https://bit.ly/3FzwXhL) descargamos la primera máquina virtual (`sys0-rocky.ova`, ~1,7 Gb) y la importamos:
01. Menú `Archivo > Importar servicio Virtualizado... `
02. Al final de la línea de entrada `Archivo`, pulsamos en el icono de selección de archivo (🗀).

    En el cuadro de diálogo que aparece, elegimos la imagen previamente descargada; pulsamos en `Seleccionar`, y luego en `Siguiente`.

03. Pulsamos en `Importar` y confirmamos el "Acuerdo de Licencia de Software" pulsando `Acepto`.

04. Cuando haya finalizado el proceso de importación, de la lista de máquinas que aparece a la izquierda seleccionaremos la máquina virtual "sys0" y abrimos su configuración (menú `Máquina > Configuración...` o bien pulsando en el icono `Configuración` (⚙) ).

05. En la ventana de configuración que aparece, seleccionamos `Red > Adaptador 1` y revisamos:

    | Conectado a              |  Nombre  |
    |--------------------------|:--------:|
    | NAT                      |    ---   |

06. Pulsamos en la pestaña `Adaptador 2` y también verificamos:

    | Conectado a              |  Nombre  |
    |--------------------------|:--------:|
    | Adaptador sólo-anfitrión | VirtualBox Host-Only Ethernet Adapter |

07. Cerramos la ventana de configuración pulsando `Aceptar`.


<br>
<br>
## Configuración de "sys0"

01. En la ventana principal de VirtualBox, seleccionamos la máquina "sys0".

02. Arrancamos la máquina virtual por medio del menú `Máquina > Iniciar > Inicio normal` (o bien pulsando sobre el  icono `Iniciar` (⇨) ).

03. Tras unos segundos acabará mostrando el _login_ por consola; accederemos con el nombre `training` y contraseña `training`.
![Ejemplo de login por consola](../../../attached_files/images/rocky8-console.png)

    > Nota: Una vez que hemos accedido al sistema estará ejecutando la _shell_ (intérprete de comandos) `bash`.  El _prompt_ `[training@sys0 ~]$ ` indica que se está a la espera para que introduzcamos algún comando.
    >
    > Por omisión (aunque es configurable) el _prompt_ muestra el usuario/a actual ("training"), el nombre de la máquina ("sys0") el directorio actual ("~", que representa nuestro directorio base o _home_) y que estamos ejecutando **sin** privilegios especiales (mediante el símbolo "$").
    >
    > A partir de ahora, en los comandos a ejecutar, vamos a simplificar e indicar el _prompt_ sólo mediante la cadena "$".  Así, si en las instrucciones vemos algo como `$ ip addr` quiere decir que debemos teclear el comando `ip addr` y pulsar `Intro` para que se ejecute.

04. Comprobamos los interfaces de red:
    ~~~
    $ ip addr
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 [...]
    link/loopback [...]
    inet 127.0.0.1/8 scope host lo
    [...]       
    2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 [...]
    link/ether [...]
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic eth0
    [...]
    3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 [...]
    link/ether [...]
    inet 192.168.56.100/24 brd 192.168.56.255 scope global noprefixroute eth1
    [...]
    ~~~

    > Nota: En la consola podemos recuperar un comando anterior pulsando la tecla "Cursor Arriba".  Después, podemos desplazarnos por la línea usando "Cursor Izda/Dcha" (o "Inicio/Fin") y modificarla si es necesario.
    >
    > Pulsando "Intro" (en cualquier punto de la línea, no necesariamente al final) se ejecuta el comando.
    >
    > Para borrar, podemos usar las teclas "Backspace" y/o "Supr.". También, "Control+U" (_undo_) borra desde la posición actual del cursor hasta el principio de la línea mientras que "Control+K" (_kill_) borra hasta el final de línea.
    >
    > Se puede hacer _scroll_ mediante "May+RePág"/"May+AvPág".

    
05. El primer interfaz (`eth0`) es el que se empleará en la máquina virtual cuando se necesite conectar con el exterior, ya que se hará traslación de direcciones (NAT) sobre el tráfico de salida.
    Para verificarlo, en el terminal de "sys0" ejecutaremos:
~~~
$ ping -I eth0 -n -c2 www.cryp.to
PING www.cryp.to (213.160.73.168) from 10.0.2.15 eth0: 56(84) bytes of data.
64 bytes from 213.160.73.168: icmp_seq=1 ttl=63 time=181 ms
64 bytes from 213.160.73.168: icmp_seq=2 ttl=63 time=176 ms
[...]
~~~

06. El segundo interfaz (`eth1`) es el que se usará para intercomunicar la máquina virtual con el anfitrión (navegador, sesión ssh, ...) y con otras máquinas virtuales (sys1, etc).

    De nuevo, podemos verificar que la configuración es correcta:
    ~~~
    $ ip addr show eth1
    3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
        link/ether 08:00:27:df:58:b1 brd ff:ff:ff:ff:ff:ff
        inet 192.168.56.100/24 brd 192.168.56.255 scope global noprefixroute eth1
           valid_lft forever preferred_lft forever
        inet6 fe80::204:33bc:4f4:970c/64 scope link noprefixroute
           valid_lft forever preferred_lft forever
    ~~~

<br>
<br>
## Comprobando la conectividad

01. Podemos intentar alcanzar el anfitrión a través de la red "VirtualBox Host-Only Ethernet Adapter":
    {% quiz %}
    ---
    locale: es
    ---

    # Conectividad MV ↪ anfitrión
    Desde la máquina virtual "sys0" lanzamos el siguiente comando, tratando de comprobar si se alcanza al anfitrión:
    ~~~
    $ ping -c2 192.168.56.1
    ~~~

    ¿Cuál será el resultado? \
    Pista: Pulsa en la bombilla

    > Lo mejor es probar el comando ... aunque el resultado **no** va a ser concluyente ;-)

    1. [ ] El anfitrión seguro que responde, porque la red "VirtualBox Host-Only Ethernet Adapter" está bien configurada.
    1. [ ] El anfitrión seguro que **no** responde: por seguridad, los anfitriones están aislados de las máquinas virtuales.
        > En otros tipos de virtualización (hipervisores, p.ej), tal vez ... pero *no* en VirtualBox, donde queremos poder emular infraestructuras completas.
    1. [x] El anfitrión *puede* que responda, o puede que no; todo depende de cómo tenga configurado el cortafuegos
        > ¡Correcto!  Es más, normalmente Windows **no** responderá al `ping`, a pesar de tener la red correctamente configurada
    {% endquiz %}


03. En el terminal de "sys0", dejamos lanzado un pequeño servidor HTML de pruebas:
~~~shell
$ sudo yum install -y python2
$ python2 -m SimpleHTTPServer
Serving HTTP on 0.0.0.0 port 8000 ...
~~~

04. **En el anfitrión** abrimos un terminal:
    * Windows: Pulsamos la tecla `Win`+`r`, tecleamos `cmd` y pulsamos `Intro`
    * Mac: Lanzamos la aplicación `Terminal`
    * Linux: Abrimos cualquiera de las aplicaciones de terminal que usemos habitualmente


05. Desde el terminal **del anfitrión** probamos, usando ICMP `ping`, la conectividad con la máquina virtual "sys0":

    ~~~shell
    $ ping 192.168.56.100
    PING 192.168.56.100 (192.168.56.100) 56(84) bytes of data.
    64 bytes from 192.168.56.100: icmp_seq=1 ttl=64 time=0.183 ms
    64 bytes from 192.168.56.100: icmp_seq=2 ttl=64 time=0.258 ms
    [...]
    ~~~
    >  Atención: en Linux/Mac tendremos que finalizar `ping` pulsando `Ctrl`+`c`

06. Siguiendo **en el anfitrión** lanzamos un navegador web y abrimos la página <http://192.168.56.100:8000>
    {% quiz %}
    ---
    locale: es
    ---
    # Conectividad anfitrión ↪ MV
    Desde el anfitrión abrimos la URL `http://192.168.56.100:8000`\
    ¿Qué se mostrará en el navegador?

    > Abre un navegador (Firefox, Edge, Chrome, etc) en el anfitrión y teclea la URL en cuestión ... ¿qué acaba mostrando el navegador?\
    Asegúrate de haber incluido el puerto `:8000`\
    Y que el comando `python -m ...` anterior aún está ejecutando

    1. [ ] Nada, porque por seguridad no puede haber tráfico entre el anfitrión y las máquinas virtuales.
    1. [ ] Nada, porque en VirtualBox no hemos habilitado el tráfico HTTP en la red "VirtualBox Host-Only Ethernet Adapter".
    1. [ ] Una página de bienvenida del (mini)servidor web.
    1. [x] El listado del directorio donde se ha lanzado el (mini)servidor web.
    {% endquiz %}

07. **Aún desde el anfitrión** abrimos una conexión ssh (recuerda que la contraseña es `training`).  Después, lanzamos algún comando y finalizamos la conexión por medio del comando `exit`:

    ~~~shell
    $ ssh training@192.168.56.100
    The authenticity of host '192.168.56.100 (192.168.56.100)' can't be established.
    ED25519 key fingerprint is SHA256:+k10Lb+sdtc7tLaMq211tuSCD1gG5CDnuBhbWKMtaWk.
    This key is not known by any other names
    Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
    Warning: Permanently added '192.168.56.100' (ED25519) to the list of known hosts.
    training@192.168.56.100's password:
    *----------------------------------------------------------------------*
    * Configured for training purposes only; not suitable for production   *
    * Este sistema ha sido configurado para su uso en cursos presenciales; *
    * no se recomienda su uso en entornos de producción.                   *
    *                                                                      *
    *   2022/12 [lmuniz@lmunix.net]                                        *
    *----------------------------------------------------------------------*
    Last login: Sun Feb 06 18:31:33 2022
    $ hostname
    sys0
    $ date
    [...]
    $ cal
    [...]
    $ exit
    ~~~

    {% quiz %}
    ---
    locale: es
    ---
    # Conexión SSH
    Antes de poder introducir la contraseña `training` hemos tenido que confirmar que queremos continuar con el proceso de conexión, introduciendo `yes`.
    ¿Por qué?
    > Pues no hemos contado nada al respecto, es cierto.  Tal vez `cat ~/.ssh/known_hosts` pueda dar una pista ...

    1. [ ] Las primeras veces se pedirá confirmación para evitar que por error nos conectemos a un sistema remoto y lancemos comando potencialmente destructivos en, p.ej, un servidor en producción.
        > ¿Qué quiere decir "las primeras veces"? ¿ 2, 5, 1000? ¡No tiene sentido!
    1. [ ] Es el comportamiento por omisión, a menos que se indique la opción `--yes`.
        > Con un `man ssh` puedes comprobar si existe el tal `--yes`.  Que va a ser que no.
    1. [x] Es la primera vez que nos conectamos contra 192.168.56.100; cuando este sistema envía su *clave pública* no se puede comparar con otra previa, así que el cliente SSH no puede saber si es correcta o no. Por eso acaba pidiendo la confirmación.
    1. [ ] Como en todos los comandos Unix/Linux, es simplemente por ~dar por c~ hacer que tecleemos más.
        > Ya, claro.  Y en las vacunas te meten un "microchís", que me lo ha dicho mi cuñao.
    {% endquiz %}

08. Volvemos a "sys0"; en la consola donde habíamos lanzado el mini servidor web, pulsamos `Ctrl`+`c` para finalizar el programa.

09. Apagamos la máquina virtual "sys0":

    ~~~shell
    $ sudo poweroff
    ~~~

    > Nota: Como alternativa, se podría perfectamente apagado desde el anfitrión (en VirtualBox, menú `Máquina > Cerrar > Apagado ACPI`.


<br>
<br>
## Instantánea (_snapshot_) de `sys0`

Una vez que hemos comprobado que la máquina `sys0` funciona y su conectividad es la correcta, tomaremos una imagen inicial (_snapshot_): así, podríamos volver a ella en caso necesario sin necesidad de volver a importarla:

1. Seleccionamos el menú `Máquina > Herramientas > Instantáneas`.

2. En parte central de la ventana aparecerá la lista de instantáneas, que sólo contendrá el llamado “⏻ Estado actual”.

3. En la barra de menús habrá aparecido una nueva entrada, que vamos a seleccionar: `Instantánea > Tomar`. En la ventana que aparece cambiamos el `Nombre de la instantánea` a "Recién instalada" y validamos pulsando en "Aceptar".

4. Podemos verificar que se ha tomado la instantánea revisando la lista de instantáneas; ahora el "⏻ Estado actual" deriva de "📷 Instantánea 1".

5. Volvemos a mostrar las características de la máquina con `Máquina > Herramientas > Detalles`.


# Conclusiones

En este ejercicio hemos preparado el entorno para el resto de laboratorios.

Hemos importado una máquina virtual y hemos comprobado que existe conectividad entre el sistema anfitrión y las máquinas que se conecten a la red interna "VirtualBox Host-Only Ethernet Adapter".

También, hemos configurado mínimamente el sistema "sys0" y hemos comprobado que puede conectarse a Internet por medio del interfaz "NAT".


<center>— ♠ —</center>

[^vb6-a]: En VirtualBox 6.x, menú `Archivo > Preferencias... > Extensiones` (en Mac, menú `VirtualBox > Preferencias... > Extensiones`)
[^vb6-b]: En VirtualBox 6.x, menú `Archivo > Administrador de red de anfitrión...` (en Mac, menú `VirtualBox > Administrador de red de anfitrión...`)