---
title: Fundamentos de Linux
subtitle: Ficheros de texto
---

# Ficheros de texto (I)

  * `cat`, `nl`, ~~`more`~~ `less`
  * `wc`
  * `head`, `tail`
  * `grep`
    * `-v`, `-i`
    * \texttt{$\wedge$ \$}
    * ...


# Ficheros de texto (II)

  * `|`, `tee [-a]`
  * `sort`, `uniq`
  * ... en general, **filtros**


# Filtros: cut

  * Corta en **vertical** (columnas)
  * `cat /etc/passwd | cut -d: -f1,6,7 | grep "bash$"`
  * `ls -l /boot | cut -c 2-10,46-`


# Filtros: tr

  * Cambia **caracteres**
  * `head /etc/passwd | tr ":" " "`
  * `cat /etc/motd | tr "[aeiou]" "[AEIOU]"`
  * también, `-d` (_delete_) y `-s` (_squeeze_)


# Filtros: sed

* Lenguaje _completo_ ...
  * ... pero facilita cambiar **unas cadenas por otras**
  * `sed -e 's/<cad>/<cad>/g'`


# Filtros: xargs

  * Convierte **líneas** en **argumentos** ...
  * ... **evita** usar **`$()`**
  * Combina muy bien con **`find`**
  * `find ~ -newer /run/utmp | xargs file`
  * ~~\texttt{find $\sim$ -name "auth*" -exec cp -v \{\} /tmp \textbackslash;}~~
  * `find ~ -name "auth*" | xargs -I{} cp -v {} /tmp`


# Filtros: awk

  * Lenguaje _completo_ ...
  * ... pero facilita cortar en **vertical** (columnas)
  * `cat /etc/passwd | awk -F: '{print $1,$7,$3}' | tail -3`
  * `ls -lh /boot | awk '{print NR": "$3,$NF,$5}'`


# Filtros: más aún

  * `join`, `paste` ~~(`lam`)~~, `split`, `csplit`
  * `printf`, `fmt`, `fold`, `expand`, `unexpand`
  * `tac`, `rev`, `shuf`
  * `iconv`, `dos2unix`, `unix2dos`


# Filtros: doc

  * Muchos filtros (y comandos _útiles_) son del paquete "coreutils"
    * `rpm -ql coreutils | grep /usr/bin`
    * `dpkg -L coreutils | grep /usr/bin`
  * Sobre `awk`: <http://bit.ly/2m7RhfK>
  * Líneas $\Longleftrightarrow$ estructuras
    * `jq`

      <http://bit.ly/2FhJxly>

      <https://bit.ly/3PLeBO5>


# grep reloaded

  * Más `grep`
    * `-w`\pause, `-c`, `-l`, `-C<num>`...
    * \texttt{$.~*~[\ldots]$}
  * `egrep`
    * `? + | () {}`
  * `fgrep`


# Ficheros binarios

  * `file`, `strings`
  * `ldd`
  * `hexdump [-C] [-v]`
  * `grep [-a] [-I]`


# Comparar

  * `cmp`, `comm`
  * `diff`, `sdiff`
    * Mejor, `diff -u`, `sdiff -l/-s`
  * `cksum`, `md5sum`, `sha*sum`, ...



# ¿Preguntas? {.plain}

  \center{\textcolor{Mahogany}{\Huge$\spadesuit$}}
