---
title: Fundamentos de Linux
subtitle: Gestión de ficheros
---

# Operaciones básicas con ficheros

::: nonincremental
  * `mv`
  * `cp`
  * `rm`
  * `ln`, `ln -s`
:::


# Mover/renombrar: mv

  * `mv <src> <dest>`
  * `mv <src> [<src> ...] <dir>`
  * Opciones
    * `-f` vs. `-i`



# Copia: cp

  * `cp <src> <dest>`
    * Se puede copiar y renombrar a la vez
  * `cp <src> [<src> ...] <dir>`
  * Opciones
    * `-v`
    * `-R` $\simeq$ ~~`-r`~~
    * `-f` vs. `-i`



# Borrado: rm

  * `rm <src> [<src> ...]`
  * Opciones
    * `-R` $=$ `-r`
    * `-f` vs. `-i`


# Enlace (físico): ln

  * `ln <src> <dest>`
  * `ln <src> [<src> ...] <dir>`
  * Opciones
    * `-v`
    * `-f` vs. `-i`



# Enlace (simbólico): ln -s

  * `ln -s <src> <dest>`
  * `ln -s <src> [<src> ...] <dir>`
  * Opciones
    * `-v`
    * `-f` vs. `-i`



# Enlaces

::: nonincremental
  * El físico **ocupa menos**
  * El físico **no se queda colgado**
  
  . . .
  
  * El simbólico **es más visible**
  * El simbólico **cruza dispositivos**
:::


# ¿Preguntas? {.plain}

  \center{\textcolor{Mahogany}{\Huge$\spadesuit$}}
