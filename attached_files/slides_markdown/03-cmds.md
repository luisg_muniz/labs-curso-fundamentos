---
title: Fundamentos de Linux
subtitle: Comandos.  Gestión de directorios
---

# Estructura de los comandos: teoría

::: nonincremental

  * comando
    * \onslide<3->{`cal` $\neq$ `CAL` $\neq$ `Cal`}
  * opciones
    * \onslide<4->{cortas: `cal -3`}
    * \onslide<5->{largas: `cal --three`}
  * argumento(s)
  * \onslide<2->todos separados por uno o varios espacios

:::


# Estructura de los comandos: ejemplo

  * `ls`
  * `ls -a /boot /proc`
  * `ls -l -a /boot` ("ele" minúscula)
    * `ls -a -l ...`
    * `ls -al ...`
    * `ls -la ...`
    * ~~`ls -la /boot -h`~~



# Rutas

  * /usr/bin, /usr/sbin
    * /usr/local/bin, $\sim$/bin
  * `which`, `echo $PATH`
  * `hash`


# Directorios

  * `pwd`, `cd`, `ls`
    * `$HOME`
    * $\sim$, $\sim$--, $\sim$usaurio
  * rutas absolutas/relativas
    * $\cdot$
    * $\cdot$$\cdot$
  * `mkdir`, `rmdir`
    * Opciones: `-v`, `-p`
  * `find`, `tree`
  * ¿`touch`?



# find: sintaxis

::: nonincremental

  * Busqueda flexible ...
  * \onslide<2->{... a partir de un origen ... }\onslide<5->{$\Rightarrow \cdot$}
  * \onslide<3->{... en base a unos criterios ...}\onslide<6->{ $\Rightarrow \forall$}
  * \onslide<4->{... y toma acciones}\onslide<7->{ $\Rightarrow$ `-print`}

:::


# find: ejemplos

::: nonincremental

  * `find /boot -type f -size +30M`
  * `find /etc -maxdepth 1 -name '*.d'`
  * `find /usr -name '*ruby*' -o \( ! -group root \) -ls`
  * \texttt{find $\sim$ -name "*bash*"}
  * \texttt{find $\sim$ -name "*bash*" -exec cp -v \{\} /tmp \textbackslash;}
    * `-ok` vs. `-exec`

:::


# ¿Preguntas? {.plain}

  \center{\textcolor{Mahogany}{\Huge$\spadesuit$}}
