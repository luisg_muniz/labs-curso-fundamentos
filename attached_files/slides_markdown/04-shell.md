---
title: Fundamentos de Linux
subtitle: Shell
---

# Shell bash

## Qué vamos a tratar

::: nonincremental
   * Comodines
   * Variables
   * Comillas
   * Redirección
   * Histórico
   * Otros
:::


# Comodines

## Comodines

   * Cualquier cadena: $\ast$
   * Un *único* carácter: ?
   * Conjuntos: $[\ldots]$
     * Por extensión (enumeración): [aeiou]
     * Por comprensión (rangos): [a-z]  [M-Z]
     * Mixtos: [0-3ab%]
     * Por exclusión: [!0246]  [^a-z]


## Más sobre comodines

   * "Expansión nula"
   * Clases
     * [[:alpha:]], [[:xdigit:]], ...
   * Comodines extendidos (_extglob_)
     * @$(\ldots)$, ?$(\ldots)$, *$(\ldots)$, ...
   * Rangos
     * `/boot/{config,sym}*`
     * `touch /tmp/r{a,e,i,o,}sa`
     * `echo dir-{100..102}`



# Variables

## Variables

   * Uso
     * `echo $USER`
     * `ls -lA $HOME`
     * `cal $i`
   * Definición
     * `var=valor [var=valor ...]`
     * `export var [var ...]`
     * `export [var[=valor] ...]`


## Más sobre variables

   * Comprobar
     * Todas: `set`
     * Entorno: `env`, `export`
   * Borrado
     * `unset var`
   * No existentes $=$ nulas $=$ vacías
   * Atributos
     * Numéricas, _read only_, _namerefs_



# Comillas

## Tipos de comillas

  * 'Simples'
  * "Dobles"
  * Protección (_escape_) con \textbackslash
  * \`Inversas\` \pause $\Rightarrow$ $(Inversas)



# Redirección

## Redirección (de E/S)

   * Salida de error
     * `2>`, `2>>`, `/dev/null`
   * Salida estándar
     * ~~`1>`~~, `>`, `>>`
   * Estándar y error
     * `2>&1`
   * Entrada estándar
     * ~~`0`~~, `<`


## Más sobre redirecciones

   * Concatenado (_pipe_)
     * `|`, `tee`
   * `set -o noclobber` $\Longleftrightarrow$ \texttt{>\!|}
   * `<<`, `<<-`
   * `<<<`



# Histórico

## Histórico (I)

   * history
     * $~\Rightarrow~\sim$`/.bash_history`
   * `set | grep "HIST"`


## Histórico (II)

::: nonincremental
   * \onslide<1->{interactivo}
     * \onslide<3->{$\gets \uparrow \downarrow \to$, C-r}
   * \onslide<2->{no interactivo}
     * \onslide<4->{!!, !-2, !<num>, !<cad>}
:::



# Otros

## Ejecución (in)condicional de comandos

   * `;`
   * \texttt{$\textbackslash\!\!\hookleftarrow$}
   * `&&`
   * `||`


## Parecidos razonables

   * `( ... )`
   * \texttt{\{\textvisiblespace...\textvisiblespace;\textvisiblespace\}}
   * `$(( ... ))`



## ¿Preguntas? {.plain}

  \center{\textcolor{Mahogany}{\Huge$\spadesuit$}}
