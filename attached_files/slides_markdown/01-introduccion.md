---
title: Fundamentos de Linux
subtitle: Introducción
---

# Qué es [Linux](https://en.wikipedia.org/wiki/History_of_Linux)

  * \onslide<1->Kernel\onslide<2->{\textsl{ y sólo kernel} $\Rightarrow$ distribución}
  * \onslide<3->{Familia Unix}
    * \onslide<5->{*BSD, macOS, AIX, HP-UX}
  * \onslide<4->{Software Libre}
    * \onslide<6->{\sout{Linux Propietary Lic.}, GPLv2\sout{v3}, GNU/Linux, \textsl{blobs}}



# Qué es una distribución

  * Instalador
  * Cargador de arranque (_bootloader_)
  * _Kernel_
  * Aplicaciones



# Distribuciones de propósito general

  * Debian 
    * \onslide<2->{Ubuntu, Linux Mint}
  * RedHat (RHEL)
    * \onslide<2->{Fedora, Centos, Rocky, Oracle Linux}
    * SuSE
      * \onslide<2->{OpenSuSE}



# Distribuciones especializadas

  * Fácil uso: Elementary OS, Pop! OS, Zorin, Manjaro
  * Configurables: Gentoo, Arch Linux, Slackware
  * Compactas: Raspbian/Pidora, DSL (Damn Small Linux), antiX, MX Linux
  * Seguridad: Qubes, Kali, WifiSlax, Tails, Whonix
  * Contenedores: Alpine, Rancher, Fedora SilverBlue
  * ...



# Plataformas

  * Intel x86\pause/x64\pause
  * ARM, PowerPC, ...
  * Embebidos: Android, routers, smart home, TVs, coches, ...
  * _Big Iron_: Mainframes, superordenadores [TOP-500](https://www.top500.org/)



# Entornos de pruebas

::: nonincremental
  * [VirtualBox](https://www.virtualbox.org/wiki)
  * MobaXterm
  * WSL (_Windows Subsistem for Linux_)
  * Remotas: ~~DistroTest~~ [OnWorks](https://www.onworks.net/)
:::



# ¿Preguntas? {.plain}

  \center{\textcolor{Mahogany}{\Huge$\spadesuit$}}
