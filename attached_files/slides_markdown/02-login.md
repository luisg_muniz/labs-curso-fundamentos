---
title: Fundamentos de Linux
subtitle: Acceso al sistema
---

# Acceso al sistema (I)

  * Login y contraseña
    * Terminales virtuales (Alt+Fn~1\ldots6~)
  * Entorno gráfico
    * Selección de distintos entornos/motores gráficos
  * Acceso remoto
    * `ssh`, X11, VNC, RDP, NoX



# Acceso al sistema (II)

  * Usuario/a:
    * `who`, `who am i`, `id`
  * Home:
    * `pwd`, `cd`, `echo $HOME`
  * Shell: bash
    * _prompt_ "[training@sys0\textvisiblespace$\sim$]$\textvisiblespace"



# Identificación del sistema

  * Kernel
    * `uname -a`
  * Distribución
    * /etc/$\ast$release$\ast$
    * /etc/$\ast$version$\ast$
  * Hardware
    * `nproc`, `free -m`, `lsblk`
    * `htop`
    * ...y muchos más



# Cierre de la sesión

  * `exit`, `logout`
  * `echo $TMOUT`
  * Secuencias de control (C-d)



# ¿Preguntas? {.plain}

  \center{\textcolor{Mahogany}{\Huge$\spadesuit$}}
