---
title: Fundamentos de Unix/Linux
description: Curso "Fundamentos de Unix/Linux"
permalink: /
---

# Curso "Fundamentos de Unix/Linux"

* Transparencias
  * Tema {% attach_file {"file_name": "/slides_pdf/01-introduccion.pdf", "title":"01: Introducción"} %}
  * Tema {% attach_file {"file_name": "/slides_pdf/02-login.pdf", "title":"02: Acceso al sistema"} %}
  * Tema {% attach_file {"file_name": "/slides_pdf/03-cmds.pdf", "title":"03: Comandos.  Gestión de directorios"} %}
  * Tema {% attach_file {"file_name": "/slides_pdf/04-shell.pdf", "title":"04: Shell"} %}
  * Tema {% attach_file {"file_name": "/slides_pdf/05-files.pdf", "title":"05: Gestión de ficheros"} %}
  * Tema {% attach_file {"file_name": "/slides_pdf/06-text.pdf", "title":"06: Ficheros de texto"} %}

